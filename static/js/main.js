$(function() {
    console.log('!');

    $('.nav__item').on('click', function() {
        var href = $(this).attr('href').slice(1);
        var sectionClass = "." + href + "__item";

        $('.main-section').fadeOut("slow", function() {
            $(this).removeClass("is-active");
        });
        $('.nav__item, .tab__item').removeClass("is-active");
        $(this).addClass("is-active");
        $('.tab__item:first-child').addClass("is-active");
        $('#' + href).fadeIn("slow", function() {
            $(this).addClass("is-active");
        });
    });

    $('.main-footer-nav__item a').on('click', function() {
        var tabId = $(this).attr('href').slice(1);
        $('.tab__item').removeClass("is-active");
        $('.main-footer-nav__item a').removeClass("is-active");
        $(this).addClass("is-active");
        $('#' + tabId).addClass("is-active");
    });

    $('.sub__tabs a').on('click', function() {
        var tabId = $(this).attr('href').slice(1);
        $('.sub__tabs a, .sub-tabs__item').removeClass("is-active");
        $(this).addClass("is-active");
        $('#' + tabId).addClass("is-active");
    });

    $('.portofino .tabs').enscroll();

    $('.gallery__item').modaal({
        type: 'image'
    });




    var menuLeft = document.getElementById('cbp-spmenu-s1'),

        body = document.body;

    showLeft.onclick = function() {
        classie.toggle(this, 'active');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        disableOther('showLeft');
    };

    function disableOther(button) {
        if (button !== 'showLeft') {
            classie.toggle(showLeft, 'disabled');
        }

    }
})
